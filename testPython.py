# -*- coding: utf-8 -*-
"""
Created on Tue Jan 25 17:32:37 2022

Functions for reformate CSV, Hestia project

@author: emrenard
"""
import pandas as pd


def reformateCSV(infile):
    """Reformate dataframe infile by merging corresponding information/node on one unique line.

    We assume that columns with names finishing by .@id can link/be linked, and that
    a name as "x.y.@id" means that x points to y. Then all columns with names
    starting with x will be merged with all columns with names starting with y
    based on columns "x.y.@id" and "y.@id".
    """
    assert isinstance(infile, pd.DataFrame), "Input should be a pandas dataframe"
    infile = infile.drop_duplicates()
    levels = buildLinkingTable(infile.columns)
    if len(levels) == 0:  # if no linking found
        return infile

    # building the new table by examining the linking levels one by one
    df = infile[infile[levels.level1[0]].notna()].dropna(axis=1, how='all')
    levels2done = []  # linking levels done
    levels2todo = list(levels.level2)  # remaining linking levels
    while len(levels2todo) > 0:
        lev2 = levels2todo[0]  # lev2 = lev1.levref
        lev1 = levels.level1[lev2]
        levref = levels.levelref[lev2]
        # print("--------------------\n df:")
        # print("lev2: %s \n levref: %s" % (lev2, levref))
        if lev1 in df.columns and levref not in df.columns:  # lev1 in df
            df2 = infile[infile[levref].notna()].dropna(axis=1, how='all')
            df = df.merge(df2, how='outer', left_on=lev2, right_on=levref).drop_duplicates()
        elif levref in df.columns and lev1 not in df.columns:  # levref in df
            df1 = infile[infile[lev1].notna()].dropna(axis=1, how='all')
            df = df1.merge(df, how='outer', left_on=lev2, right_on=levref).drop_duplicates()
        elif levref not in df.columns and lev1 not in df.columns:  # none in df
            df2 = infile[infile[lev1].notna()].dropna(axis=1, how='all')
            df = pd.concat([df, df2])
        else:  # both levels already in df
            if len(df[lev2].compare(df[levref])) > 0:
                raise ValueError("Incompatible columns %s and %s" % (lev2, levref))
        levels2todo.remove(lev2)
        levels2done.append(lev2)
        # print("Remaining columns to see: %s" % levels2todo)
    df = df[infile.columns]
    return df


def buildLinkingTable(colnames):
    """From a list of names, extracts the (oriented) links between them.

    We assume that only names finishing by .@id can link/be linked, and that
    if a name as "x.y.@id" means that x points to y.
    Returns a pd.DataFrame with columns level2, level1 et levelref with level2
    giving the 'linking' name, and level1 and levelref give the linked name.
    If level2 = x.y.@id, then level1 = x.@id and levelref = y.@id.
    If no link is found, then an empty list is returned.
    """
    # building table of linking/linked levels
    colid = [i for i in colnames if i.endswith('.@id')]
    # if no columns 'id'
    if len(colid) == 0:
        print("No column name finishing by .@id to use for merging")
        return []
    # finding the levels (highest and 'linking' levels)  by number of dot
    colLev = {j: len([i for i in j if i == '.']) for j in colid}
    colLev1 = [i for i in colLev.keys() if colLev[i] == 1]  # highest level
    colLev2 = [i for i in colLev.keys() if colLev[i] >= 2]  # others levels
    levels = pd.DataFrame(index=colLev2, columns=['level2', 'level1', 'levelref'])
    levels.level2 = colLev2
    levels.level1 = ["%s.@id" % i.split('.')[0] for i in levels.level2]
    levels.levelref = ["%s.@id" % i.split('.')[1] for i in levels.level2]
    # if some highest level is unconnected to any other one
    lev1_sep = [i1 for i1 in colLev1 if not levels.isin([i1]).any().any()]
    levels_sup = pd.DataFrame(index=lev1_sep, columns=['level2', 'level1', 'levelref'])
    for i in lev1_sep:  #
        levels_sup.loc[i] = i
    levels = levels.append(levels_sup)
    # defaultSource <-> source
    levels.levelref.replace('defaultSource.@id', 'source.@id', inplace=True)
    # if levelref not present in highest level, remove the corresponding line
    levels = levels.iloc[[i for (i, j) in enumerate(levels.levelref) if j in colLev1], :]
    if levels.shape[0] == 0:
        print("No column names linking two highest nodes present in the dataset")
        return []

    return levels
