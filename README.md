# Hestia-test

Test python Hestia (Oxford University)

Reformatting a CSV file to make it easier to read.

See https://gitlab.com/hestia-earth/hestia-schema/-/snippets/2194329 and https://www.hestia.earth/schema/Cycle.

Main function *reformateCSV* in *testPython.py* and tests of this function in *test_reformateCSV.py*.


