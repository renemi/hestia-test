# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 22:30:33 2022

Tests for function reformateCSV

@author: emrenard
"""

import pandas as pd
import numpy as np
from testPython import reformateCSV


def prepare(infile):
    """Preprocess dataframe infile"""
    torepl = {'true': True, 'false': False, '-': np.nan}
    infile = infile.replace(torepl, value=None)
    colNum = [i for i in infile.columns if i.endswith('value') or i.endswith('Quantity')]
    colDate = [i for i in infile.columns if i.endswith('Date')]
    infile[colNum] = infile[colNum].astype('float').round(6)
    infile[colDate] = infile[colDate].astype('string')
    infile = infile.dropna(axis=0, how='all').dropna(axis=1, how='all')
    return infile


def compareDF(df1, df2):
    """Compare two dataframes df1 and df2 and returns their difference.

    Check columns names, shapes, removes columns/rows full of missing values,
    match columns and row orders and returns the difference between both using
    pandas.compare.
    """
    assert df1.shape == df2.shape, "Dataframes of different size"
    assert set(df1.columns) == set(df2.columns), "Dataframes with different columns"
    df1 = df1.drop_duplicates().dropna(axis=0, how='all').dropna(axis=1, how='all')
    df2 = df2.drop_duplicates().dropna(axis=0, how='all').dropna(axis=1, how='all')
    df2 = df2[df1.columns]  # same columns order
    df1 = df1.sort_values(by=list(df1.columns))  # same rows order
    df2 = df2.sort_values(by=list(df2.columns))
    df2.index = df1.index
    diff = df1.compare(df2)
    return diff


# load and preproces examples files
pathfilein = r"input.csv"
pathfileout = r"output.csv"
outfile = prepare(pd.read_csv(pathfileout))
infile = prepare(pd.read_csv(pathfilein))


# check given example
def test_exampleGiven():
    infile_r = reformateCSV(infile)
    diff = compareDF(infile_r, outfile)
    assert diff.shape == (0, 0)


# if multiple connex components
def test_twoConnexComponents():
    dfsup = infile.copy()
    colid = [i for i in dfsup.columns if i.endswith('.@id')]
    dfsup[colid] = dfsup[colid] + '_test'
    df = pd.concat([infile, dfsup], axis=0, ignore_index=True)
    infile_2 = reformateCSV(df)
    out2 = outfile.copy()
    out2[colid] = out2[colid] + '_test'
    outfile_2 = pd.concat([outfile, out2], axis=0, ignore_index=True)
    diff = compareDF(infile_2, outfile_2)
    assert diff.shape == (0, 0)


# if one 'highest' level unconnected to any other
def test2_unconnectedHighestLevel():
    colsup = ('lev1sup.@id', 'lev1sup.name', 'lev1sup.info')
    dfsup = pd.DataFrame('test', columns=colsup, index=['rowsup'])
    df = pd.concat([infile, dfsup], axis=1)
    infile_r = reformateCSV(df)
    outfile_r = pd.concat([outfile, dfsup])
    diff = compareDF(infile_r, outfile_r)
    assert diff.shape == (0, 0)


# if weird columns names (no ., or @id)
def test_inputFormat():
    df = infile.copy()
    df.columns = [i.replace('id', 'ID') for i in df.columns]
    infile_r = reformateCSV(df)
    assert compareDF(infile_r, df).shape == (0, 0)

    df = infile.copy()
    df.columns = [i.replace('.', 'dot') for i in df.columns]
    df.columns = [i.replace('dot@', '.@') for i in df.columns]
    infile_r = reformateCSV(df)
    assert compareDF(infile_r, df).shape == (0, 0)
